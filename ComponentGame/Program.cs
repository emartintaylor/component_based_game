﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Factory.ComponentFactory factory = Factory.ComponentFactory.getInstance();
            ComponentGame.Components.HealthComponent health = factory.getHealthComponent(10);

            health.action(new Messages.ChangeHealthMessage(5));

            Console.ReadKey();
        }
    }
}
