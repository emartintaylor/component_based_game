﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ComponentGame.Components;
using ComponentGame.Messages;

namespace ComponentGame
{
    class GameObject
    {
        /// <summary>
        /// Counter to assign IDs to components
        /// </summary>
        private static int IDCounter = 0;

        /// <summary>
        /// Unique ID of the component
        /// </summary>
        private int ID = 0;

        /// <summary>
        /// List of all components
        /// </summary>
        private List<Component> components = null;

        /// <summary>
        /// Creates the GameObject
        /// </summary>
        public GameObject()
        {
            components = new List<Component>();

            ID = IDCounter++;
        }

        /// <summary>
        /// Adds a component to the GameObject
        /// </summary>
        /// <param name="component">Component to be added</param>
        public void AddComponent(Component component)
        {
            components.Add(component);
        }

        /// <summary>
        /// Removes a component from the GameObject
        /// </summary>
        /// <param name="component"></param>
        public void RemoveComponent(Component component)
        {
            components.Remove(component);
        }

        /// <summary>
        /// Sends the message to all of the components
        /// </summary>
        /// <param name="message">Message to send</param>
        public void RecieveMessage(IMessage message)
        {
            dynamic _message = (dynamic)message;
            foreach(Component component in components)
            {
                component.action(_message);
            }
        }

        /// <summary>
        /// String output of the object
        /// </summary>
        /// <returns>ID to uniquely ID the GameObject</returns>
        public override string ToString()
        {
            return GetType().ToString() + " ID=" + ID;
        }

        public void tick()
        {
            foreach(Component component in components)
            {
                component.tick();
            }
        }


    }
}
