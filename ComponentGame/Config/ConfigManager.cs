﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame.Config
{
    interface ConfigManager
    {
        void initialize();

        /// <summary>
        /// Get a configurtion value
        /// </summary>
        /// <param name="configKey">Key used to get the configuration value</param>
        /// <returns></returns>
        string getConfig(string configKey);
    }
}
