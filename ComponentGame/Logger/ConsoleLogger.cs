﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame.Logger
{
    class ConsoleLogger : ILogger
    {
        /// <summary>
        /// Static version of the class
        /// </summary>
        private static ConsoleLogger logger = null;

        /// <summary>
        /// Creates a singleton of this class
        /// </summary>
        /// <returns>Gets the single instance of this class</returns>
        public ILogger getInstance()
        {
            if(logger == null)
            {
                logger = new ConsoleLogger();
            }

            return logger;
        }

        /// <summary>
        /// Logs the instance to the console
        /// </summary>
        /// <param name="Class"></param>
        /// <param name="Method"></param>
        /// <param name="message"></param>
        public void Log(string message, [System.Runtime.CompilerServices.CallerMemberName] string methodName = "", [System.Runtime.CompilerServices.CallerFilePath] string className = "" )
        {   
            Console.WriteLine(className + " - " + methodName + " - " + message);
        }
    }
}
