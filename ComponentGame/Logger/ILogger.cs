﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame.Logger
{
    enum LogLevel
    {
        Debug,
        Info,
        Error
    }
    interface ILogger
    {
        /// <summary>
        /// Interface for the Logging class
        /// </summary>
        /// <returns></returns>
        ILogger getInstance();
        void Log(string message, string methodName="", string className="");
    }
}
