﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentGame.Logger;
using System.Diagnostics;

namespace ComponentGame
{
    static class GameLogger
    {
        private static ILogger _logger;
        public static ILogger logger
        {
            get
            {
                if(_logger == null)
                {
                    _logger = new ConsoleLogger();
                }
                return _logger;
            }
            set
            {
                _logger = value;
            }
        }

        public static void Log(string message, LOGLEVEL level = LOGLEVEL.TRACE, [System.Runtime.CompilerServices.CallerMemberName] string methodName = "", [System.Runtime.CompilerServices.CallerFilePath] string className = "")
        {
            logger.Log(message, methodName, className.Split('\\').LastOrDefault());
        }
    }
}
