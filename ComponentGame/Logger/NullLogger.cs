﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame.Logger
{
    class NullLogger : ILogger
    {
        private static NullLogger logger = null;

        public ILogger getInstance()
        {
            if (logger == null)
            {
                logger = new NullLogger();
            }

            return logger;
        }

        public void Log(string message)
        {
            
        }

        public void Log(string message, string test, string test2)
        {

        }
    }
}
