﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame.Components
{
    class HealthComponent : Component
    {
        private int _health = 0;
        private int _maxHealth = 0;
        

        public HealthComponent(int health, int maxHealth):base()
        {
            GameLogger.Log(String.Format("New Health - Health: {0} Max Health: {1}", health, maxHealth));
            CurrentHealth = health;
            MaxHealth = maxHealth;
        }

        public HealthComponent(int health):this(health, health)
        {

        }
        public HealthComponent():this(0)
        { }

        public int CurrentHealth
        {
            get { return _health; }
            protected set {

                GameLogger.Log("Health currently " + CurrentHealth+", Max Health: "+MaxHealth+", Health Update: "+value);
                if (value < 0)
                {
                    _health = 0;
                }
                else if (value > MaxHealth)
                {
                    _health = MaxHealth;
                }
                else
                {
                    _health = value;
                }
                GameLogger.Log("Health changed to " + CurrentHealth, Logger.LOGLEVEL.DEBUG);
            }
        }

        public int MaxHealth
        {
            get { return _maxHealth; }
            protected set
            {
                if (value < 0)
                    _maxHealth = 0;
                else
                    _maxHealth = value;

                if(MaxHealth < CurrentHealth)
                {
                    CurrentHealth = MaxHealth;
                }
                GameLogger.Log("Max Health="+MaxHealth+" - Health changed to " + CurrentHealth);
            }

        }


        public void action(Messages.ChangeHealthMessage changeHealth)
        {
            CurrentHealth += changeHealth.deltaHealth;
            GameLogger.Log("Max Health=" + MaxHealth + " - Health changed to " + CurrentHealth);
        }
    }
}
