﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentGame.Messages;

namespace ComponentGame.Components
{
    abstract class Component
    {
        protected List<IMessage> messages = new List<IMessage>();


        public void action(IMessage message)
        {
            GameLogger.Log("No action")
;        }
        
        public void tick()
        {

        }

        public override Boolean Equals(Object obj)
        {
            if(obj == null)
            {
                return false;
            }

            if(obj.GetType() == this.GetType())
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
