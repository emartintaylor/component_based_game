﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentGame.Messages
{
    class ChangeHealthMessage
    {
        public int deltaHealth {get; set;}

        public ChangeHealthMessage(int changeAmount = 10)
        {
            deltaHealth = changeAmount;
        }
    }
}
