﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentGame.Components;


namespace ComponentGame.Factory
{
    class ComponentFactory : IFactory
    {
        private static ComponentFactory factory = null;   

        public static ComponentFactory getInstance()
        {
            if(factory == null)
            {
                factory = new ComponentFactory();
                GameLogger.logger = new ComponentGame.Logger.ConsoleLogger();                
            }
            return factory;
        }

        public HealthComponent getHealthComponent(int health = 10, int maxHealth = 10)
        {

            HealthComponent healthComp = new HealthComponent(health, maxHealth);

            return healthComp;
        }
    }
}
